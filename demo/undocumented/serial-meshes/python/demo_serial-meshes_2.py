"""This demo ilustrates a feature (for now experimantal) how to build
serial mesh, function space and function by every process and use this
function for evaluation in distributed forms on non-matching mesh.

Note that this can do useful job as f may be fed by arbitrary data (from
file, pipe or whatever) instead of interpolated from simple Expression.

Run by commands:
$ np=3                                         # or whatever you want
$ mpirun -n $np python demo_serial-meshes_2.py
$ paraview u.pvd
"""

# Copyright (C) 2013 Jan Blechta
#
# This file is part of DOLFIN.
#
# DOLFIN is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DOLFIN is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
#
# First added:  2013-02-04
# Last changed: 2013-02-06

from dolfin import *

# TODO: Need to be tested on Epetra.
parameters["linear_algebra_backend"] = 'PETSc'

# Create serial mesh, function space and some function in every process
parameters['pretend_one_process'] = True
mesh = UnitSquareMesh(4, 4)
V = FunctionSpace(mesh, 'CG', 1)
f = interpolate(Expression("sin(2*pi*x[0]) * cos(2*pi*x[1])"), V)
parameters['pretend_one_process'] = False

# Wrapper for serial function to be used in parallel, see below
class SerialFunctionWrapper(Expression):
    def __init__(self, f):
        self.f = f
        Expression.__init__(self)
    def eval(self, values, x):
        return self.f.eval(values, x)

# Same wrapper but in C++
wrapper_code = '''
class SerialFunctionWrapper : public Expression
{
public:

  boost::shared_ptr<Function> f;

  SerialFunctionWrapper() : Expression()
  {
  }

// We don't use eval_cell because this is intended
// to be evaluated in mesh non-matching with f.
void eval(Array<double>& values, const Array<double>& x) const
  {
      f->eval(values, x);
  }
};'''

# Define poisson problem in parallel on non-matching mesh
mesh = UnitSquareMesh(6, 6)
V = FunctionSpace(mesh, 'CG', 1)
bc = DirichletBC(V, 0, "on_boundary")
u = TrialFunction(V)
v = TestFunction(V)
a = inner(grad(u), grad(v))*dx
# L = f*v*dx
#     This would cause assembler to call Function::update() on f which
#     in turn calls PETScVector::update_ghost_values() because it sees
#     MPI::num_processes() > 1. As f is serial and has no ghost values,
#     PETSc will throw some errors. This is (for now) hacked by wrapping
#     serial function f into expression f_wrap:

# python wrapper
#f_wrap = SerialFunctionWrapper(f)

# C++ wrapper, much faster evaluation on assembly than python wrapper
f_wrap = Expression(wrapper_code)
f_wrap.f = f

L = f_wrap*v*dx

# Solve
u = Function(V)
solve(a == L, u, bcs=bc)

# Plot
File('u.pvd') << u
