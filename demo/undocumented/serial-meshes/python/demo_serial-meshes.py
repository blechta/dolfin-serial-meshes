"""This demo ilustrates a feature (for now experimantal) to build mesh,
function space and function by only one process while running in parallel.
See blueprint https://blueprints.launchpad.net/dolfin/+spec/serial-meshes.

Moreover projection on non-matching meshes can be performed by one process
while this is not possible (for now) in parallel. See bug
https://bugs.launchpad.net/dolfin/+bug/1047430.

Run by commands:
$ np=3                                       # or whatever you want
$ mpirun -n $np python demo_serial-meshes.py
$ paraview u_4x4.pvd
$ paraview u_6x6.pvd
"""

# Copyright (C) 2013 Jan Blechta
#
# This file is part of DOLFIN.
#
# DOLFIN is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DOLFIN is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
#
# First added:  2013-02-03

from dolfin import *

# TODO: Need to be tested on Epetra.
parameters["linear_algebra_backend"] = 'PETSc'

# Create parallel mesh, function space and some function
mesh = UnitSquareMesh(4, 4)
V = FunctionSpace(mesh, 'CG', 1)
u = interpolate(Expression("sin(2*pi*x[0]) * cos(2*pi*x[1])"), V)

# Plot
File('u_4x4.pvd') << u

# Export function data to file
# TODO: consider using pipes (where supported by os)
File('temp_func.xml') << u

# Now compute by only one process
if MPI.process_number() == 0:

    # Let dolfin believe that there is only one process
    parameters['pretend_one_process'] = True

    # Make same mesh and function space as above, but on only one process
    mesh = UnitSquareMesh(4, 4)
    V = FunctionSpace(mesh, 'CG', 1)

    # Initialize serial function
    u = Function(V, 'temp_func.xml')

    # Make another serial mesh and function space
    mesh2 = UnitSquareMesh(6, 6)
    V2 = FunctionSpace(mesh2, 'CG', 1)

    # Project serially
    u2 = project(u, V2)

    # Write out result
    File('temp_func.xml') << u2

    # Now let dolfin think we have more processes
    parameters['pretend_one_process'] = False

# Wait until process 0 finishes its bussiness
# TODO: is it necessary?
MPI.barrier()

# Make parallel mesh and function space as the last ones in serial
mesh2 = UnitSquareMesh(6, 6)
V2 = FunctionSpace(mesh2, 'CG', 1)

# Inialize parallel function
u2 = Function(V2, 'temp_func.xml')

# Plot
File('u_6x6.pvd') << u2
